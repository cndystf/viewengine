//isinya router doang
import express from "express";
import { mainRoute } from "./controller/basicController.js";
import { checkHealth } from "./controller/checkHealthController";
import { seeBio } from "./controller/biodataController";
import { checkAuthKey } from "./middleware/checkKey.js";
import { get as getProduct, create as createProduct,
update as updateProduct, destroy as deleteProduct,
find as findProduct } from "./controller/productController.js"
// import { basicMiddleware } from "./middleware/basicMiddleware.js";


const router = express.Router()

router.get('/',mainRoute)
router.get('/check-health', checkHealth)
router.get('/biodata', checkAuthKey, seeBio)

export default router